<?php

use Illuminate\Database\Seeder;

class ImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('workspace_images')->insert([
            'name' => 'lada',
            'src' => 'images/lada.png',
            'styles' => json_encode([
                'top' => '200px',
                'left' => '200px',
                'width' => 'auto',
                'zIndex' => '1',
            ]),
        ]);

        DB::table('workspace_images')->insert([
            'name' => 'pancake',
            'src' => 'images/pancake.png',
            'styles' => json_encode([
                'top' => '900px',
                'left' => '2500px',
                'width' => '200px',
                'zIndex' => '3',
            ]),
        ]);

        $this->command->info('Images seeding successful.');
    }
}
