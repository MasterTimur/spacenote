<?php

use Illuminate\Database\Seeder;

class NoteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('workspace_notes')->insert([
            'name' => 'Ремонт Жигулей',
            'body' => '<ul><li>1. Починить реле</li><li>2. Починить подвеску</li><li>3. Починить двигатель</li><li>4. Починить кресло</li><li>5. Починить стекло</li></ul>',
            'styles' => json_encode([
                'top' => '100px',
                'left' => '500px',
                'zIndex' => '3',
            ]),
            'published' => true,
        ]);

        DB::table('workspace_notes')->insert([
            'name' => 'Что я хочу сегодня кушать',
            'body' => '<p>Я бы съел блинчик. Со сметаной. И дал бы коту блинчик со сметаной. Если закончится сметана, то придется идти в магазин.</p><br><p>А я не хочу в магазин. И блинчиков ещё много.</p>',
            'styles' => json_encode([
                'top' => '2160px',
                'left' => '750px',
                'zIndex' => '4',
            ]),
            'published' => true,
        ]);

        $this->command->info('Notes seeding successful.');
    }
}
