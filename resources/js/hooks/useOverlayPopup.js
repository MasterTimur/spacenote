import { useState } from 'react';

const useOverlayPopup = () => {
  const [preview, setPreview] = useState(null);
  const [isNotePreviewOpened, setIsNotePreviewOpened] = useState(false);

  const openPreview = (data) => {
    setPreview(data);
    setIsNotePreviewOpened(true);
  };

  return {
    preview,
    openPreview,
    isNotePreviewOpened,
  };
};

export default useOverlayPopup;
