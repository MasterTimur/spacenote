import { useState } from 'react';

const useNewImage = () => {
  const [newImage, setNewImage] = useState(null);
  const [newImageName, setNewImageName] = useState(null);

  return {
    setNewImage,
    setNewImageName,
    newImage,
    newImageName,
  };
};

export default useNewImage;
