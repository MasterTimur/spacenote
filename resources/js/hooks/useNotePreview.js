import { useState } from 'react';

const useNotePreview = () => {
  const [preview, setPreview] = useState(null);
  const [name, setName] = useState(null);
  const [noteId, setNoteId] = useState(null);
  const [isNotePreviewOpened, setIsNotePreviewOpened] = useState(false);
  const [isNoteEditorOpened, setIsNoteEditorOpened] = useState(false);

  const openPreview = (
    body,
    newName,
    id,
  ) => {
    setPreview(body);
    setName(newName);
    setNoteId(id);
    setIsNotePreviewOpened(true);
  };

  return {
    preview,
    name,
    noteId,
    openPreview,
    isNotePreviewOpened,
    setIsNotePreviewOpened,
    isNoteEditorOpened,
    setIsNoteEditorOpened,
  };
};

export default useNotePreview;
