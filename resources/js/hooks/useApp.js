import { useState, useRef } from 'react';

import {
  useContextMenu,
  useDataFill,
  useNewImage,
  useNotePreview,
} from './exports';

import { sendImageData, sendNoteData } from '../helpers/sendData';

const useApp = () => {
  const refWrapper = useRef(null);

  const [isOpen, setIsOpen] = useState(false);
  const [isTextEditorOpen, setIsTextEditorOpen] = useState(false);
  const [isImageFieldOpen, setIsImageFieldOpen] = useState(false);

  const [newNoteData, setNewNoteData] = useState(null);
  const [newNoteTitle, setNewNoteTitle] = useState('Новая заметка');

  const {
    xPos,
    yPos,
    menu,
  } = useContextMenu(refWrapper);

  const {
    preview,
    name,
    noteId,
    openPreview,
    isNotePreviewOpened,
    setIsNotePreviewOpened,
    isNoteEditorOpened,
    setIsNoteEditorOpened,
  } = useNotePreview();

  const {
    imagesData,
    notesData,
    saveImageData,
    saveNoteData,
    getNotesData,
    getImagesData,
  } = useDataFill();

  const {
    setNewImage,
    setNewImageName,
    newImage,
    newImageName,
  } = useNewImage();

  const openTextEditor = () => {
    setIsOpen(true);
    setIsTextEditorOpen(true);
  };

  const triggerInputImage = () => {
    setIsOpen(true);
    setIsImageFieldOpen(true);
  };

  async function onFilesChange(files) {
    setNewImage(await toBase64(files[0]));
    setNewImageName(files[0].name);
  }

  const handleSave = () => {
    isImageFieldOpen && saveImage();
    isTextEditorOpen && saveNote();

    setIsOpen(false);
    setIsTextEditorOpen(false);
    setIsImageFieldOpen(false);
    setNewNoteData(null);
    setNewImage(null);
  };

  const saveNote = () => {
    const savedNote = {
      name: newNoteTitle,
      body: newNoteData,
      styles: JSON.stringify({
        top: yPos,
        left: xPos,
      }),
    };

    saveNoteData(savedNote);
    sendNoteData(savedNote);
  };

  const saveImage = () => {
    const savedImage = {
      name: newImageName,
      src: newImage,
      styles: JSON.stringify({
        'top': yPos,
        'left': xPos,
        'width': '200px',
      }),
    };

    saveImageData(savedImage);
    sendImageData(savedImage);
  };

  const toBase64 = (file) => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });

  const closePopup = () => {
    setIsOpen(false);
    setIsTextEditorOpen(false);
    setIsImageFieldOpen(false);
  };

  const isImageField = isImageFieldOpen && !newImage;

  return {
    refWrapper,
    xPos,
    yPos,
    menu,
    isOpen,
    preview,
    name,
    noteId,
    openPreview,
    isNotePreviewOpened,
    setIsNotePreviewOpened,
    isNoteEditorOpened,
    setIsNoteEditorOpened,
    imagesData,
    notesData,
    newImage,
    openTextEditor,
    triggerInputImage,
    onFilesChange,
    handleSave,
    closePopup,
    isImageField,
    isTextEditorOpen,
    setNewNoteData,
    setNewNoteTitle,
    newNoteTitle,
    getNotesData,
    getImagesData,
  };
};

export default useApp;
