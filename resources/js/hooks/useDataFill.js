import { useEffect, useState } from 'react';
import axios from 'axios';

const useDataFill = () => {
  const [imagesData, setImagesData] = useState(null);
  const [notesData, setNotesData] = useState(null);

  const saveNoteData = (savedNote) => {
    setNotesData([
      ...notesData,
      savedNote,
    ]);
  };

  const saveImageData = (savedImage) => {
    setImagesData([
      ...imagesData,
      savedImage,
    ]);
  };

  const getImagesData = () => {
    axios.get('get-image-list/')
      .then((response) => {
        setImagesData(response.data);
      });
  };

  const getNotesData = () => {
    axios.get('get-note-list/')
      .then((response) => {
        setNotesData(response.data);
      });
  };

  useEffect(() => {
    getNotesData();
    getImagesData();
  }, []);

  return {
    imagesData,
    notesData,
    saveImageData,
    saveNoteData,
    getNotesData,
    getImagesData,
  };
};

export default useDataFill;
