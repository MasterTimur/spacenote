import React from 'react';

import Image from '../components/Image/Image';

const renderImages = (
  imagesData,
  newStyles,
  getImagesData,
  draggingOn,
  setDraggingOn,
) => {
  const imageElements = imagesData.map(({
    id,
    src,
    name,
    styles,
  }) => (
    <Image
      id={id}
      src={src}
      alt={name}
      styles={styles}
      newStyles={newStyles}
      getImagesData={getImagesData}
      draggingOn={draggingOn}
      setDraggingOn={setDraggingOn}
      key={id}
    />
  ));

  return imageElements;
};

export default renderImages;
