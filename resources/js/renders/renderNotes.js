import React from 'react';

import Note from '../components/Note/Note';

const renderNotes = (
  notesData,
  openPreview,
  getNotesData,
  newStyles,
  draggingOn,
  setDraggingOn,
) => {
  const notesElements = notesData.map(({
    id,
    name,
    body,
    styles,
  }) => (
    <Note
      id={id}
      name={name}
      body={body}
      styles={styles}
      openPreview={openPreview}
      getNotesData={getNotesData}
      newStyles={newStyles}
      draggingOn={draggingOn}
      setDraggingOn={setDraggingOn}
      key={id}
    />
  ));

  return notesElements;
};

export default renderNotes;
