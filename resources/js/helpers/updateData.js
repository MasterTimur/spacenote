import axios from 'axios';

export const updateNoteData = (id, body, name, published) => {
  axios.post(
    `get-note-list/${id}`,
    {
      id,
      body,
      name,
      published,
    },
  );
};

export const updateNoteStyles = (id, styles, published = true) => {
  axios.post(
    `get-note-list/${id}`,
    {
      styles,
      published,
    },
  );
};

export const updateImageStyles = (id, styles) => {
  axios.post(
    `get-image-list/${id}`,
    {
      styles,
    },
  );
};
