import axios from 'axios';

export const sendImageData = (savedImage) => {
  axios.post('get-image-list/', {
    name: savedImage.name,
    src: savedImage.src,
    styles: savedImage.styles,
  });
};

export const sendNoteData = (savedNote) => {
  axios.post('get-note-list/', {
    name: savedNote.name,
    body: savedNote.body,
    styles: savedNote.styles,
    published: true,
  });
};
