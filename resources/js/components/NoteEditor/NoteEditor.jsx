import React, { useState } from 'react';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import PropTypes from 'prop-types';

import Overlay from '../Overlay/Overlay';

import { updateNoteData } from '../../helpers/updateData';

const NoteEditor = ({
  data,
  name,
  id,
  setIsNoteEditorOpened,
  getNotesData,
}) => {
  const [editedNoteData, setEditedNoteData] = useState(null);
  const [editedNoteName, setEditedNoteName] = useState(null);

  const saveEditedNoteData = (published = true) => {
    updateNoteData(id, editedNoteData, editedNoteName, published);
    getNotesData();
    setIsNoteEditorOpened(false);
  };

  const handleChange = (e) => {
    setEditedNoteName(e.target.value);
  };

  return (
    <Overlay>
      <div className="cropper-wrapper">
        <div className="note-editor-title">
          <input
            type="text"
            defaultValue={name}
            onChange={(e) => handleChange(e)}
          />
        </div>
        <CKEditor
          editor={ClassicEditor}
          data={data}
          config={
            {
              ckfinder: {
                // eslint-disable-next-line max-len
                uploadUrl: 'https://example.com/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images&responseType=json',
              },
            }
          }
          onChange={(event, editor) => {
            const myTest = editor.getData();
            setEditedNoteData(myTest);
          }}
        />
      </div>
      <button
        onClick={() => saveEditedNoteData()}
        type="button"
      >
        Save
      </button>
      <button
        onClick={() => setIsNoteEditorOpened(false)}
        type="button"
      >
        Close
      </button>
      <button
        onClick={() => saveEditedNoteData(false)}
        type="button"
      >
        Delete
      </button>
    </Overlay>
  );
};

NoteEditor.propTypes = {
  data: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  id: PropTypes.number.isRequired,
  setIsNoteEditorOpened: PropTypes.func.isRequired,
  getNotesData: PropTypes.func.isRequired,
};

export default NoteEditor;
