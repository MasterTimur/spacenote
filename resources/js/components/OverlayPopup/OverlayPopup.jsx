import React from 'react';
import PropTypes from 'prop-types';

import TextEditor from '../TextEditor/TextEditor';
import ImageEditor from '../ImageEditor/ImageEditor';
import PopupButtons from '../PopupButtons/PopupButtons';

import './OverlayPopup.scss';

const OverlayPopup = ({
  isTextEditorOpen,
  isImageField,
  newImage,
  setNewNoteData,
  setNewNoteTitle,
  newNoteTitle,
  onFilesChange,
  handleSave,
  closePopup,
}) => (
  <div className="overlay">
    <div className="overlay-popup">
      <div className="cropper-wrapper">
        {isTextEditorOpen && (
          <TextEditor
            setNewNoteData={setNewNoteData}
            setNewNoteTitle={setNewNoteTitle}
            newNoteTitle={newNoteTitle}
          />
        )}
        {isImageField && (
          <ImageEditor
            onFilesChange={onFilesChange}
          />
        )}
        {newImage && (
          <img
            src={newImage}
            alt="gears"
            style={{ 'width': '200px' }}
          />
        )}
      </div>
      <PopupButtons
        handleSave={handleSave}
        closePopup={closePopup}
      />
    </div>
  </div>
);

OverlayPopup.propTypes = {
  isTextEditorOpen: PropTypes.bool.isRequired,
  isImageField: PropTypes.bool.isRequired,
  newImage: PropTypes.string.isRequired,
  setNewNoteData: PropTypes.func.isRequired,
  setNewNoteTitle: PropTypes.func.isRequired,
  newNoteTitle: PropTypes.string.isRequired,
  onFilesChange: PropTypes.func.isRequired,
  handleSave: PropTypes.func.isRequired,
  closePopup: PropTypes.func.isRequired,
};

export default OverlayPopup;
