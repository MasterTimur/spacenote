import React from 'react';
import PropTypes from 'prop-types';

import './PopupButtons.scss';

const PopupButtons = ({
  handleSave,
  closePopup,
}) => (
  <div className="cropper-buttons">
    <button
      className="cropper-buttons-save"
      type="button"
      onClick={() => handleSave()}
    >
      Сохранить
    </button>
    <button
      className="cropper-buttons-cancel"
      type="button"
      onClick={() => closePopup()}
    >
      Отмена
    </button>
  </div>
);

PopupButtons.propTypes = {
  handleSave: PropTypes.func.isRequired,
  closePopup: PropTypes.func.isRequired,
};

export default PopupButtons;
