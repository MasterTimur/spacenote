import React, { useState } from 'react';
import PropTypes from 'prop-types';

import { updateImageStyles } from '../../helpers/updateData';

const Image = ({
  id,
  src,
  name,
  styles,
  newStyles,
  getImagesData,
  draggingOn,
  setDraggingOn,
}) => {
  const [isDragging, setIsDragging] = useState(false);
  const [stylesState, setStylesState] = useState(styles);

  const handleDoubleClick = () => {
    if (draggingOn) { return; }

    setIsDragging(!isDragging);

    if (isDragging) {
      updateImageStyles(id, newStyles);
      getImagesData();
      setDraggingOn(false);
    }

    setStylesState(newStyles);
  };

  return (
    <img
      src={src}
      alt={name}
      style={JSON.parse(isDragging ? newStyles : stylesState)}
      className="spacenote-image"
      onDoubleClick={() => handleDoubleClick()}
    />
  );
};

Image.propTypes = {
  id: PropTypes.number.isRequired,
  src: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  styles: PropTypes.string.isRequired,
  newStyles: PropTypes.string.isRequired,
  getImagesData: PropTypes.func.isRequired,
  draggingOn: PropTypes.bool.isRequired,
  setDraggingOn: PropTypes.func.isRequired,
};

export default Image;
