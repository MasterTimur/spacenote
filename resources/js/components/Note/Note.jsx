import React, { useState } from 'react';
import ReactHtmlParser from 'react-html-parser';
import PropTypes from 'prop-types';

import { updateNoteStyles } from '../../helpers/updateData';

const Note = ({
  id,
  name,
  body,
  styles,
  openPreview,
  getNotesData,
  newStyles,
  draggingOn,
  setDraggingOn,
}) => {
  const [isDragging, setIsDragging] = useState(false);
  const [stylesState, setStylesState] = useState(styles);

  const handlePreview = () => {
    openPreview(body, name, id);
  };

  const handleDoubleClick = () => {
    if (draggingOn) { return; }

    setIsDragging(!isDragging);

    if (isDragging) {
      updateNoteStyles(id, newStyles);
      getNotesData();
      setDraggingOn(false);
    }

    setStylesState(newStyles);
  };

  return (
    <div
      className="note-wrapper"
      key={id}
      style={JSON.parse(isDragging ? newStyles : stylesState)}
      onDoubleClick={() => handleDoubleClick()}
      role="presentation"
    >
      <h4>{name}</h4>
      <div className="card-separator" />
      <div className="card-list">
        {ReactHtmlParser(body)}
      </div>
      <button
        type="button"
        onClick={() => handlePreview()}
      >
        Превью
      </button>
    </div>
  );
};

Note.propTypes = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  body: PropTypes.string.isRequired,
  styles: PropTypes.string.isRequired,
  openPreview: PropTypes.bool.isRequired,
  getNotesData: PropTypes.func.isRequired,
  newStyles: PropTypes.string.isRequired,
  draggingOn: PropTypes.bool.isRequired,
  setDraggingOn: PropTypes.func.isRequired,
};

export default Note;
