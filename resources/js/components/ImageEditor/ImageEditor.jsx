import React from 'react';
import Files from 'react-files';
import PropTypes from 'prop-types';

const ImageEditor = ({ onFilesChange }) => (
  <div>
    <Files
      className="files-dropzone"
      onChange={onFilesChange}
      accepts={['image/png']}
      multiple
      maxFiles={1}
      maxFileSize={10000000}
      minFileSize={0}
      clickable
    >
      <div>
        Закиньте сюда картинку или нажмите чтобы загрузить
      </div>
    </Files>
  </div>
);

ImageEditor.propTypes = {
  onFilesChange: PropTypes.func.isRequired,
};

export default ImageEditor;
