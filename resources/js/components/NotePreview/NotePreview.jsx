import React from 'react';
import ReactHtmlParser from 'react-html-parser';
import PropTypes from 'prop-types';

import Overlay from '../Overlay/Overlay';

import './NotePreview.scss';

const NotePreview = ({
  preview,
  name,
  setIsNotePreviewOpened,
  setIsNoteEditorOpened,
}) => {
  const handleEdit = () => {
    setIsNotePreviewOpened(false);
    setIsNoteEditorOpened(true);
  };

  return (
    <Overlay>
      <div className="cropper-wrapper">
        <div className="note-preview-name">
          <h1>
            {name}
          </h1>
        </div>
        <div className="note-preview-body">
          {ReactHtmlParser(preview)}
        </div>
      </div>
      <button
        onClick={() => setIsNotePreviewOpened(false)}
        type="button"
      >
        Close
      </button>
      <button
        onClick={() => handleEdit()}
        type="button"
      >
        Edit
      </button>
    </Overlay>
  );
};

NotePreview.propTypes = {
  preview: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  setIsNotePreviewOpened: PropTypes.func.isRequired,
  setIsNoteEditorOpened: PropTypes.func.isRequired,
};

export default NotePreview;
