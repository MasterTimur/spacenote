import React from 'react';
import PropTypes from 'prop-types';

import './Overlay.scss';

const Overlay = ({ children }) => (
  <div className="overlay">
    <div className="overlay-popup">
      {children}
    </div>
  </div>
);

Overlay.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Overlay;
