import React, { useState } from 'react';
import { MapInteractionCSS } from 'react-map-interaction';

import useApp from '../hooks/useApp';
import { renderImages, renderNotes } from '../renders/exports';

import {
  ContextMenu,
  OverlayPopup,
  NotePreview,
  NoteEditor,
} from './exports';

import './clear.scss';
import './App.scss';

function App() {
  const {
    refWrapper,
    xPos,
    yPos,
    menu,
    isOpen,
    preview,
    name,
    noteId,
    openPreview,
    isNotePreviewOpened,
    setIsNotePreviewOpened,
    isNoteEditorOpened,
    setIsNoteEditorOpened,
    imagesData,
    notesData,
    newImage,
    openTextEditor,
    triggerInputImage,
    onFilesChange,
    handleSave,
    closePopup,
    isImageField,
    isTextEditorOpen,
    setNewNoteData,
    setNewNoteTitle,
    newNoteTitle,
    getNotesData,
    getImagesData,
  } = useApp();

  const [newStyles, setNewStyles] = useState(null);

  const [value, setValue] = useState({
    scale: 1,
    translation: { x: 0, y: 0 },
  });

  const [dragginOn, setDraggingOn] = useState(false);

  const translationBounds = {
    xMin: -99999,
    xMax: 0,
    yMin: -99999,
    yMax: 0,
  };

  const handleWrapperMouseMove = (e) => {
    const bounds = refWrapper.current.getBoundingClientRect();
    const eventXPos = e.clientX - bounds.left;
    const eventYPos = e.clientY - bounds.top;

    setNewStyles(JSON.stringify({
      top: eventYPos / value.scale - 50,
      left: eventXPos / value.scale - 50,
      zIndex: 9999,
    }));
  };

  return (
    <div className="App">
      <MapInteractionCSS
        translationBounds={translationBounds}
        value={value}
        onChange={(position) => setValue(position)}
        showControls
      >
        <div
          className="wrapper"
          ref={refWrapper}
          style={{ 'backgroundImage': 'url(./images/background.jpg)' }}
          onMouseMove={(e) => handleWrapperMouseMove(e)}
        >
          {menu && (
            <ContextMenu
              xPos={xPos}
              yPos={yPos}
              triggerInputImage={triggerInputImage}
              openTextEditor={openTextEditor}
            />
          )}
          {imagesData && renderImages(imagesData, newStyles, getImagesData, dragginOn, setDraggingOn)}
          {notesData && renderNotes(notesData, openPreview, getNotesData, newStyles, dragginOn, setDraggingOn)}
        </div>
      </MapInteractionCSS>
      {isOpen && (
        <OverlayPopup
          isTextEditorOpen={isTextEditorOpen}
          isImageField={isImageField}
          newImage={newImage}
          setNewNoteData={setNewNoteData}
          setNewNoteTitle={setNewNoteTitle}
          newNoteTitle={newNoteTitle}
          onFilesChange={onFilesChange}
          handleSave={handleSave}
          closePopup={closePopup}
        />
      )}
      {isNotePreviewOpened && (
        <NotePreview
          preview={preview}
          name={name}
          setIsNotePreviewOpened={setIsNotePreviewOpened}
          setIsNoteEditorOpened={setIsNoteEditorOpened}
        />
      )}
      {isNoteEditorOpened && (
        <NoteEditor
          data={preview}
          name={name}
          id={noteId}
          setIsNoteEditorOpened={setIsNoteEditorOpened}
          getNotesData={getNotesData}
        />
      )}
    </div>
  );
}

export default App;
