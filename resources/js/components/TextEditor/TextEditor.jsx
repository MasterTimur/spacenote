import React from 'react';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import PropTypes from 'prop-types';

const TextEditor = ({
  setNewNoteData,
  setNewNoteTitle,
  newNoteTitle,
}) => {
  const handleChange = (e) => {
    setNewNoteTitle(e.target.value);
  };

  return (
    <div>
      <div className="note-editor-title">
        <input
          type="text"
          defaultValue={newNoteTitle}
          onChange={(e) => handleChange(e)}
        />
      </div>
      <CKEditor
        editor={ClassicEditor}
        config={
          {
            ckfinder: {
              // eslint-disable-next-line max-len
              uploadUrl: 'https://example.com/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images&responseType=json',
            },
          }
        }
        onChange={(event, editor) => {
          const data = editor.getData();
          setNewNoteData(data);
        }}
      />
    </div>
  );
};

TextEditor.propTypes = {
  setNewNoteData: PropTypes.func.isRequired,
  setNewNoteTitle: PropTypes.func.isRequired,
  newNoteTitle: PropTypes.string.isRequired,
};

export default TextEditor;
