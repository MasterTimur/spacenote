import React from 'react';
import PropTypes from 'prop-types';

import './ContextMenu.scss';

const ContextMenu = ({
  xPos,
  yPos,
  triggerInputImage,
  openTextEditor,
}) => {
  const style = {
    top: yPos,
    left: xPos,
  };

  return (
    <ul
      className="spacenote-context-menu"
      style={style}
    >
      <li
        onClick={() => triggerInputImage()}
        role="presentation"
      >
        Создать картинку
      </li>
      <li
        onClick={() => openTextEditor()}
        role="presentation"
      >
        Создать заметку
      </li>
    </ul>
  );
};

ContextMenu.propTypes = {
  xPos: PropTypes.string.isRequired,
  yPos: PropTypes.string.isRequired,
  triggerInputImage: PropTypes.func.isRequired,
  openTextEditor: PropTypes.func.isRequired,
};

export default ContextMenu;
