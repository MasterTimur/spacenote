<?php

namespace App\Http\Controllers;

use App\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class ImageController extends Controller
{
    public function show()
    {
       $image = Image::all();

       return response()->json($image, 200);
    }

    public function store(Request $request)
    {
        $image = $request->src;
        $image_name = $request->name;
        $styles = $request->styles;

        $image = str_replace('data:image/png;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $image = base64_decode($image);

        $image_file_name = 'image-' . time() . '.png';

        Storage::put('images/' . $image_file_name, $image);

        $url = Storage::url($image_file_name);

        \DB::table('workspace_images')->insert([
            'name' => $image_name,
            'src' => $url,
            'styles' => $styles,
        ]);

        return response()->json('is ok');
    }

    public function update(Request $request, $id)
    {
        $image = Image::where('id', $id)->first();
        $image->styles = $request->styles ? $request->styles : $image->styles;
        $image->save();

        return response()->json($image);
    }
}
