<?php

namespace App\Http\Controllers;

use App\Note;
use Illuminate\Http\Request;

class NoteController extends Controller
{
    public function show()
    {
       $note = Note::where('published', true)->get();

       return response()->json($note, 200);
    }

    public function store(Request $request)
    {
        \DB::table('workspace_notes')->insert([
            'name' => $request->name,
            'body' => $request->body,
            'styles' => $request->styles,
        ]);

        return response()->json('is ok');
    }

    public function update(Request $request, $id)
    {
        $note = Note::where('id', $id)->first();
        $note->name = $request->name ? $request->name : $note->name;
        $note->body = $request->body ? $request->body : $note->body;
        $note->styles = $request->styles ? $request->styles : $note->styles;
        $note->published = $request->published;
        $note->save();

        return response()->json($note);
    }
}
