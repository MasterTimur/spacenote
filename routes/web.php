<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\ImageController;
use App\Http\Controllers\NoteController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome', ['users' => App\User::all()]);
});

Route::get('/get-image-list','ImageController@show');
Route::post('/get-image-list','ImageController@store');
Route::post('/get-image-list/{id}','ImageController@update');

Route::get('/get-note-list','NoteController@show');
Route::post('/get-note-list','NoteController@store');
Route::post('/get-note-list/{id}', 'NoteController@update');
